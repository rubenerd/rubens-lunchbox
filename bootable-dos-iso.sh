#!/bin/sh

######
## bootable-dos-iso.sh
##
## Create bootable DOS ISO with a floppy image and source directory 
## for legacy x86 systems (pre-1999). Thank you Joerg Schilling for this 
## excellent tooling <3!
##
## AllBootDisks.com is a great source of DOS boot disks. Or you can easily
## make on an existing DOS system with format /s
##
## NOTES:
## - cp437 is the input charset for DOS; iso8859-1 for others
## - ISO level 1 limits file names to 8.3 chars
## - (non-standard) joliet records useful for Win95 and NT
## - posix-H follows symlinks, rather than coding them in
## - Rock Ridge extensions not useful in DOS etc, but doesn't harm!
##
## Modern machines should use:
## -no-emul-boot -boot-load-seg 1984 -boot-load-size 4 -joliet-long \
## -full-iso9660-filenames
## 

set -e

_BOOT_DISK_IMAGE="$1"                     ## eg: ./msdos6.img
_OUTPUT_FILE="$4"                         ## eg: "NewBootDisk(.iso)"
_PREPARER="Made by $(id -F), $(date -R)"  ## eg: Name and date
_SOURCE_DIR="$2"                          ## eg: /usr/home/rubenerd/dos/
_VOLUME_NAME="$3"                         ## eg: BOOTDISK

if [ $# -ne 4 ]; then
    printf "%s" "Usage: dos-bootable-iso.sh <boot disk image> "
    printf "%s\\n" "<source dir> <volume label> <ISO file>"
    exit 1
elif ! command -v mkisofs &> /dev/null; then
    printf "%s\n" "Error: mkisofs/cdrecord needs to be installed"
    exit 1
fi

mkisofs                                \
    -eltorito-boot "$_BOOT_DISK_IMAGE" \
    -eltorito-catalog "BOOT.CAT"       \
    -eltorito-platform "x86"           \
    -input-charset "cp437"             \
    -iso-level 1                       \
    -joliet                            \
    -log-file "$_OUTPUT_FILE.log"      \
    -output "$_OUTPUT_FILE.iso"        \
    -preparer "$_PREPARER"             \
    -publisher "$_PREPARER"            \
    -posix-H                           \
    -rational-rock                     \
    -volset "$_VOLUME_NAME"            \
    "$_SOURCE_DIR"

if command -v qemu-system-i386 &> /dev/null; then
    printf "%s\n" "Test booting with QEMU? (y/n)?"

    case $(read _TEST) in
        [Yy]* ) qemu-system-i386 -machine isapc -m 64M -cdrom "$_OUTPUT_FILE.iso"
    esac
fi

