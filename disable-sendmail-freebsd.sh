#!/bin/sh

######
## Well and truly disable sendmail on a FreeBSD host
## 2020-12

sysrc sendmail_enable="NO" 
sysrc sendmail_submit_enable="NO"
sysrc sendmail_outbound_enable="NO"
sysrc sendmail_msp_queue_enable="NO"
