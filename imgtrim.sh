#!/bin/sh

######
## imgtrim.sh
## Remove blank space from around images

set -e

_SRC="$1"

magick convert "$1" -verbose -fuzz 5% -trim +repage "trimmed-$1"

