#!/usr/bin/env perl

######
## signatures.pl
## Trying Perl signatures from Perl 5.20
## https://perldoc.perl.org/feature#The-'signatures'-feature

use strict;
use warnings;

use feature qw(say signatures);
no warnings qw(experimental::signatures);

sub sumificate ($left, $right) {
  return $left + $right;
}

sub oldertwo {
  my ($left, $right) = @_;
  return $left + $right;
}

sub olderone {
  my $message = shift;
  return $message;
}

say &sumificate(42,58);
say &oldertwo(42,58);
say &olderone("Hello, world");

