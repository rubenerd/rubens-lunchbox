#!/usr/bin/env python3

"""
tkintertest3.py: Simple Tkinter test in Python3
2021-03-18

You should get a window:

+-------------------------------+
|  This is Tcl/Tk version x.x   |
|  This should be a cedilla: ç  |
|  < Click Me! >                |
|  < QUIT >                     |
+-------------------------------+

"""

import tkinter as tk

def main():
    tk._test()

if __name__ == "__main__":
    main()

